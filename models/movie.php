<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;

class Movie extends ActiveRecord 
{
	public static function tableName()
	{
		return 'movies';
	}

	public function rules() // חוקי וולידציה
	{
		return 
		[
			[['movieName','ganre','minAge','grade'],'string','max'=>255],
			[['movieName','ganre','minAge','grade'], 'required'],
			[['movieName'],'unique'],
		];
	}
  
      public function attributeLabels()
    {
        return [
            'movieName' => 'Movie Name',
            'ganre' => 'Ganre',
            'minAge' => 'Min Age',
            'grade' => 'Grade',

        ];
    }
    /**
     * @inheritdoc
     */

}
?>